
'use strict';

(function () {
    'use strict';
    angular.module("app").controller("bookListController", bookListController);
    bookListController.$inject = ['$scope', '$state', 'appService'];


    function bookListController($scope, $state, appService) {
        var vm = this;

        vm.loadBookList = function () {
            appService.fetchBookDetails().then(function (bookDetail) {
                vm.bookList = bookDetail.data;
                appService.fetchCategoryDetails().then(function (categoryDetail) {
                    vm.category = categoryDetail.data;
                    angular.forEach(vm.bookList, function (book) {
                        var categoryName = vm.category.find((genre) => {
                            return book.CategoryId == genre.CategoryId;
                        })
                        book.GenreName = categoryName.CategoryGenre;
                    })
                }) 
            })
        }


        vm.booksCopy = angular.copy(vm.bookList);

        vm.editBook = function (book) {
            vm.goToAddBook(book.BookId);
        }

        vm.deleteBook = function (book) {
            let checkBookId = vm.bookList.find((value) => {
                return value.BookId == book.BookId;
            })
            if (checkBookId) {
                window.alert('Your book entry will be deleted');
                appService.deleteBookDetails(book.BookId).then(function () {
                    vm.loadBookList();
                })
            }
        }

        vm.addBook = function (book) {
            let checkBookId = vm.bookList.find((value) => {
                return value.BookId == book.BookId;
            })
            if (checkBookId) {
                window.alert('ABC');
            }
            else {
               var addNewBook = {
                    CategoryId: book.CategoryId,
                    BookName: book.BookName,
                    BookAuthor: book.BookAuthor,
                    BookPublisher: book.BookPublisher,
                    BookPrice: book.BookPrice,
                    BookQuantity: book.BookQuantity,
                    BookAbout: book.BookAbout,
                    BookId: bookList.BookId,
                };
                console.log(addNewBook);
                appService.setBookDetails(addNewBook).then(function () {
                })
            }
        }

        vm.goToAddBook = function (id) {
            var url = $state.href('app3');
            if (id) {
                window.open(url + '?id=' + id.toString(), '_self');
            }
            else {
                window.open(url, '_self');
            }
        }

        vm.goHome = function () {
            var url = $state.href('app');
            window.open(url, '_self');
        }
    }
})()