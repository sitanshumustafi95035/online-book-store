(function () {
    'use strict';
    angular.module("app").controller('checkoutController', checkoutController);
    checkoutController.$inject = ['$scope', '$state', 'appService'];

    function checkoutController($scope, $state, appService) {
        var vm = this;

        vm.loadCheckout = function () {
            appService.fetchCartItemsDetails().then(function (cartItemDetail) {
                vm.cartItems = cartItemDetail.data;
                vm.calculateTotal();
            })
        }

        vm.bagTotal = 0;
        vm.discount = 10;
        vm.tax = 5;
        vm.orderTotal = 0;
        vm.deliveryCharges = 150;
        vm.total = 0;

        vm.itemQuantityInc = function (id) {
            vm.cartItems.forEach((item) => {
                if (id == item.ItemBookId) {
                    item.ItemQuantity++;
                    vm.calculateTotal();
                }
            })
        }

        vm.itemQuantityDec = function (id) {
            vm.cartItems.forEach((item) => {
                if (id == item.ItemBookId) {
                    if (item.ItemQuantity <= 0) {
                        window.alert('Book will be removed from the cart');
                        appService.deleteCartItemsDetails(id).then(function () {
                            appService.fetchCartItemsDetails().then(function (cartItemDetail) {
                                vm.cartItems = cartItemDetail.data;
                                vm.calculateTotal();
                            })
                        })
                    } else {
                        item.ItemQuantity--;
                        vm.calculateTotal();
                    }
                }
            })
        }

        vm.calculateTotal = function () {
            vm.bagTotal = 0;
            vm.orderTotal = 0;
            vm.total = 0;

            vm.cartItems.forEach((item) => {
                vm.bagTotal = vm.bagTotal + (item.ItemPrice * item.ItemQuantity);
            })

            vm.discountedAmount = vm.bagTotal / vm.discount;
            vm.orderTotal = (vm.bagTotal - vm.discountedAmount);
            vm.orderTotal = vm.orderTotal + (vm.orderTotal / vm.tax);
            vm.total = vm.orderTotal + vm.deliveryCharges;
        }

        vm.goHome = function () {
            var url = $state.href('app');
            window.open(url, '_self');
        }

        vm.proceedPayment = function () {
            window.alert('Thank you for shopping with us');
            appService.emptyCartItems().then(function () {
                appService.fetchCartItemsDetails().then(function (cartItemDetail) {
                    vm.cartItems = cartItemDetail.data;
                })
            })
            vm.goHome();
        }
    };
})()