

(function () {
    'use strict';
    angular.module("app").controller("landingPageController", landingPageController);
    landingPageController.$inject = ['$scope', '$state', 'appService'];

    function landingPageController($scope, $state, appService) {
        var vm = this;
        vm.bookStoreUser = function () {
            appService.fetchBookDetails().then(function (bookDetail) {
                vm.bookList = bookDetail.data;
                vm.booksCopy = angular.copy(vm.bookList);
                appService.fetchCategoryDetails().then(function (categoryDetail) {
                    vm.category = categoryDetail.data;
                    appService.fetchCartItemsDetails().then(function (cartItemDetail) {
                        vm.cartItems = cartItemDetail.data;
                        if (vm.cartItems) {
                            vm.count = vm.cartItems.length;
                        }
                        else {
                            vm.count = 0;
                        }
                    })
                })
            })
        }


        vm.addToCart = function (book) {
            let checkId = vm.cartItems.find((value) => {
                return value.ItemBookId == book.BookId;
            })
            if (checkId) {
                window.alert('This book is already in your cart');
            }
            else {

                book = {
                    ItemBookId: book.BookId,
                    ItemName: book.BookName,
                    ItemAuthor: book.BookAuthor,
                    ItemPublisher: book.BookPublisher,
                    ItemPrice: book.BookPrice,
                    ItemQuantity: book.BookQuantity,
                    ItemAbout: book.BookAbout

                }
                appService.setCartItemsDetails(book).then(function () {
                })
                vm.count++;
                window.alert('Cart Updated');
            }
        }

        vm.genreBookList = function (genreId) {
            if (genreId === 1) {
                vm.bookList = angular.copy(vm.booksCopy);
            } else {
                vm.bookList = vm.booksCopy.filter((book) => {
                    return genreId === book.CategoryId;
                })
            }
        }

        vm.goToCheckout = function () {
            var url = $state.href('app1');
            window.open(url, '_self');
        }
    }
})()




