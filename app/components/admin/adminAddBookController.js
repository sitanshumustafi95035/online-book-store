(function () {
    'use strict';
    angular.module("app").controller("adminAddBookController", adminAddBookController);
    adminAddBookController.$inject = ['$scope', '$state', '$location', 'appService'];

    function adminAddBookController($scope, $state, $location, appService) {
        var vm = this;
        var urlParams = $location.search();


        vm.loadAddBook = function () {
            appService.fetchBookDetails().then(function (bookDetail) {
                vm.bookList = bookDetail.data;
                appService.fetchCategoryDetails().then(function (categoryDetail) {
                    vm.category = categoryDetail.data;
                    vm.getId(urlParams.id);
                })
            })
            vm.newBook = {};
        }



        vm.updateBookList = function (book) {
            if (typeof urlParams == "string") {
                var book = {
                    BookId: `${urlParams.id}`,
                    // BookId: book.BookId,
                    BookName: book.BookName,
                    BookAuthor: book.BookAuthor,
                    BookPublisher: book.BookPublisher,
                    BookPrice: book.BookPrice,
                    BookQuantity: book.BookQuantity,
                    BookAbout: book.BookAbout,
                    CategoryId: book.CategoryGenre.CategoryId,
                };
                // console.log(book);
                appService.updateBookDetails(book).then(function () {
                    var url = $state.href('app2');
                    window.open(url, '_self');
                })
            }
            else {
                appService.deleteBookDetails(`${urlParams.id}`).then(function () {
                })
                vm.newBook.BookName = book.BookName;
                vm.newBook.BookAuthor = book.BookAuthor;
                vm.newBook.BookPublisher = book.BookPublisher;
                vm.newBook.BookPrice = book.BookPrice;
                vm.newBook.BookAbout = book.BookAbout;
                vm.newBook.CategoryId = book.CategoryGenre.CategoryId;
                appService.setBookDetails(vm.newBook).then(function () {
                    var url = $state.href('app2');
                    window.open(url, '_self');
                })
                // console.log(vm.newBook);
            }
        }

        vm.getId = function (id) {
            let editBook = vm.bookList.find((book) => {
                return book.BookId == id;
            })
            if (editBook) {
                var getCategoryGenre = vm.category.find((genre) => {
                    return editBook.CategoryId == genre.CategoryId;
                })
                vm.newBook.CategoryGenre = getCategoryGenre;
                vm.newBook.CategoryId = getCategoryGenre.CategoryId;
                vm.newBook.BookName = editBook.BookName;
                vm.newBook.BookAuthor = editBook.BookAuthor;
                vm.newBook.BookPublisher = editBook.BookPublisher;
                vm.newBook.BookPrice = editBook.BookPrice;
                vm.newBook.BookAbout = editBook.BookAbout;
            }
        }
    }
})()