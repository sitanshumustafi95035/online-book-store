'use strict';

(function(angular){
	'use strict';
	var app = angular.module("app",['ui.router', 'ngSanitize']);
	app.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider){	    
	    $stateProvider.state('app', {
            url: '/',
            templateUrl: 'app/components/userBookStore/landingPage.html',
            controller: 'landingPageController as $lpc'
        }).state('app1', {
            url: '/checkout',
            templateUrl: 'app/components/checkout/checkout.html',
            controller: 'checkoutController as $coc'
        }).state('app2', {
            url: '/admin',
            templateUrl: 'app/components/bookList/bookList.html',
            controller: 'bookListController as $bkc'
        }).state('app3', {
            url: '/adminAddBook',
            templateUrl: 'app/components/admin/adminAddBook.html',
            controller: 'adminAddBookController as $abc'
        });
        $urlRouterProvider.otherwise('/');
	}])
})(window.angular);
