(function () {
  'use strict';
  angular.module("app").service("appService", appService);
  appService.$inject = ["$http"];

  function appService($http) {

    return {
      fetchBookDetails,
      fetchCategoryDetails,
      fetchCartItemsDetails,
      setBookDetails,
      // setCategoriesDetails,
      setCartItemsDetails,
      updateBookDetails,
      deleteBookDetails,
      deleteCartItemsDetails,
      emptyCartItems
    }
    
    function fetchBookDetails() {
      return $http({
        method: 'GET',

        url: 'http://localhost/bookstore/api/bookstore'
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    function fetchCategoryDetails() {
      return $http({
        method: 'GET',
        url: 'http://localhost/bookstore/api/categories'
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    function fetchCartItemsDetails() {
      return $http({
        method: 'GET',
        url: 'http://localhost/bookstore/api/cartitems'
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    function setBookDetails(book) {
      return $http({
        method: 'POST',

        url: 'http://localhost/bookstore/api/bookstore',
        data: book
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    //   function setCategoriesDetails(categories) {
    //     return $http({
    //         method: 'POST',

    //         url: 'http://localhost/bookstore/api/categories',
    //         body: {
    //           CategoryId: categories.CategoryId,
    //           CategoryGenre: categories.CategoriesGenre
    //         }
    //       }).then(function successCallback(response) {
    //           // this callback will be called asynchronously
    //           // when the response is available
    //           return response;
    //         }, function errorCallback(response) {
    //           // called asynchronously if an error occurs
    //           // or server returns response with an error status.
    //         });
    // }

    function setCartItemsDetails(book) {
      return $http({
        method: 'POST',
        url: 'http://localhost/bookstore/api/cartItems',
        data: book
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    function updateBookDetails(book) {
      return $http({
        method: 'PUT',
        url: `http://localhost/bookstore/api/bookstore/${book.BookId}`,
        data: book
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
      });
    }

    function deleteBookDetails(id) {
      return $http({
        method: 'DELETE',
        url: `http://localhost/bookstore/api/bookstore/${id}`,
      }).then(function successCallback(response) {        
        return response;
      }, function errorCallback(response) {       
      });
    }

    function deleteCartItemsDetails(id) {
      return $http({
        method: 'DELETE',
        url: `http://localhost/bookstore/api/cartItems/${id}`,
      }).then(function successCallback(response) {        
        return response;
      }, function errorCallback(response) {       
      });
    }

    function emptyCartItems() {
      return $http({
        method: 'DELETE',
        url: 'http://localhost/bookstore/api/cartItems/empty_cart',
      }).then(function successCallback(response) {        
        return response;
      }, function errorCallback(response) {       
      });
    }

  }
})()
